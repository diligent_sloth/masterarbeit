
# for sourcing functions



GetDfAttributes <- function(db) {


  # Set df-Variables according to Dataset Objects (Global)
  # get Name of Dataset
  assign("df.name", db$attributes$name, envir = .GlobalEnv)

  # needed for identification purposes
  assign("country",db$attributes$country,  envir = .GlobalEnv)
  assign("school.id", db$attributes$school.id, envir = .GlobalEnv)

  # any number of control variables
  assign("control.vars", c(country, school.id), envir = .GlobalEnv)

}


GetSequenceCols <- function(df, df.name, seq.start, seq.stop, control.vars) {

  # identify column-numbers of given column-names in dataset
  seq.name <- paste0(df.name, "_", seq.start, "_", seq.stop)
  seq.freq <- paste0(seq.name, "_freq")

  seq.start <- match(seq.start, names(df))
  seq.stop <- match(seq.stop, names(df))
  control.vars <- match(control.vars, names(df))

  #get length of sequence range
  seq.len <- ncol(df[, seq.start:seq.stop])

  return(list(seq.start, seq.stop, control.vars, seq.len, seq.name, seq.freq))
}



GetUrps <- function(df, df.1, seq.name, seq.len, seq.freq) {

  # define Vector of urps by getting the number of factor-levels in first column
  # of df and multiply it with length of sequence. Returning selection of URPs
  # Args: df
  #       df1 = df to append to append to

  urp <<- strrep(c(1: nlevels(df[, 1])), seq.len)

  df.urp <- df.1[df.1[, seq.name] %in% urp, ]
  df.urp <- GetFrequencies(df.urp, seq.name, seq.freq, 2)

  return(df.urp)
}



concatItems <- function(df, start, stop, name, append=FALSE){
  # create a column with a string which contains the concatenated range of
  # items. This column is either appended to the existing dataframe or replaces
  # the given range of columns


  # convert list of factors to Matrix of Integers (get rid of factor levels)
  concat.list <- data.matrix(df[start:stop])

  # Set "NA" to X
  concat.list[is.na(concat.list)] <- "X"

  # make list again
  concat.list <- as.data.frame(concat.list)

  concat.list <- as.vector(do.call(paste0, concat.list))


  if (append == FALSE){

    # remove all but the excluded columns (usually control.vars)
    df <- df[, -c(start:stop)]

  }

  df <- cbind(df, concat.list)

  # merged sequence is no factor, thus converting it to simple characters
  df[, ncol(df)] <- as.character(df[, ncol(df)])

  # rename the column of merged sequence
  colnames(df)[(ncol(df))] <- name

  return(df)

}



DeleteNa <- function(df, n = 0) {

  # elimnating rows with more than "n" NAs.
  # is always applied to last column

  df[str_count(df[, ncol(df)], "X") < n, ]
}



GetFrequencies <- function(df, name, freq, min) {
  # returning frequencies of specified field. freq provides Columnname

  freq.table <- count(df[, eval(name)])
  colnames(freq.table) <- c(name, freq)
  df <- join(df, freq.table )
  df <- df[df[freq] >= min, ]
  df <- droplevels(df)

  return(df)
}



CleanUp <- function(df, df.name){
  # removing Unwanted Cases (Hardcoded for each Dataset)
  # For PISA 2015: exclude Spain (of 201 occurences, 195 duplicates)
  # For Pisa 2009: Remove wrong coded nonrespondents in Brazil and Panama, and France completely (no Data at all)

  if (df.name == Pisa15$attributes$name) {
    
    df <- df[df[, 2] != "Spain",]

  } else if (df.name == Pisa09$attributes$name) {

    df[df[, 2] == "Brazil" & df[, 5] %in% c("00120", "00227", "00455", "00456", "00457", "00579", "00704", "00752", "00879", "00945", "00017"), 5:ncol(df)] <- NA
    df[df[, 2] == "Panama" & df[, 5] %in% c("00026", "00068", "00074", "00121", "00160", "00185", "00016"), 5:ncol(df)] <- NA
    df <- df[df[, 2] != "France",]
    
  }

  df <- droplevels(df)
  
  return(df)
}


GetIntraCntFreq <- function(df, seq.name, seq.freq, country, min) {
  # count the combinations of 'Sequence x country'

  df <- count(df, c(seq.name, country))

  colnames(df)[3] <- seq.freq

  df <- df[df[seq.freq] >= min, ]

  return(df)
}


GetIntraCntSummary <- function(df, seq.freq, country) {
  # sum up all frequency fields for each country

  df <- aggregate(df[, seq.freq], list(df[, country]), sum)

  colnames(df) <- c(country, seq.freq)

  return(df)
}



GetSummary <- function(df, df.attachment, df.name, seq.name, seq.freq) {

  if (exists("df.summary") == TRUE){

    df.summary <- join(df.summary, df.attachment)
    df.summary[, paste0(seq.name, "_rel")] <- df.summary[, seq.freq] / df.summary$Absolute_Cnt

  } else {

    #get Overall Frequency of Countries
    df.summary <- count(df, country)
    colnames(df.summary)[2] <- paste0("Absolute_Cnt_", df.name)

    df.summary <- join(df.summary, df.attachment, by = country)
    df.summary[, paste0(seq.name, "_rel")] <- df.summary[, seq.freq] / df.summary$Absolute_Cnt

  }

  return(df.summary)
}



ColorPal <- function(n){

  # create color Palette for n Items
  colors <- RColorBrewer::brewer.pal(12, "Paired")
  colorpal <- colorRampPalette(colors)
  colorpal <- colorpal(n)
}
