library("intsvy")
library("foreign")
library("plyr")
library("rstudioapi")

setwd(dirname(rstudioapi::getActiveDocumentContext()$path))

source("./utility_functions.R")
source("./datasets.R")
source("./levenshtein.R")



# the Dataset for which the distances should be computed
datasets <- list(Pisa00, Pisa03, Pisa06, Pisa09, Pisa12, Pisa15)

ptm <- proc.time()

df.item.list <- list()
df.item.list.summary <- list()



for (db in datasets) {

  # Read SPSS source file
  dataset <-
    read.spss(db$attributes$path, use.value.labels = TRUE, to.data.frame = TRUE)

  GetDfAttributes(db)

  # exclude duplicated or unwanted cases
  dataset <- CleanUp(dataset, df.name)


  # get colnumbers of those Items which should be contained in string (defined by
  # db$lev.ranges)
  seq <- lapply(db$lev.ranges, function(x) match(x, names(dataset)))

  df.control <- dataset[, control.vars]

  #initialize list and counter for storing results of loop
  df.list <- list()
  n <- 0

  #subset data
  for (i in seq) {
    n <- n + 1
    df.list[[n]] <- dataset[, i[[1]]:i[[2]]]

  }

  df <- as.data.frame(df.list)

  #initialize list for storing results of loop
  df.items <- as.data.frame(names(df[1:ncol(df)]))

  names(df.items) <- "Name of Item"


  for (i in 1:ncol(df)) {

    item.levels <- length(levels(df[, i]))

    df.items[i, "Number of Options"] <- item.levels

  }


  write.xlsx2(
    df.items,
    file = paste0("../OUTPUT/answering_options.xlsx"),
    sheetName = db$attributes$name,
    append = TRUE
  )

  #write frequencies
  write.xlsx2(
    table(df.items[,2]),
    file = paste0("../OUTPUT/answering_options.xlsx"),
    sheetName = paste0(db$attributes$name, "_freq"),
    append = TRUE
  )

}

proc.time() - ptm
